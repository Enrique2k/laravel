@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card-header">
            <form method="get" action="/products" >
                <input name="price" type="text"/>
                <input value="search" type="submit"/>
            </form>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Products') }}</div>
                    <div class="card-body">
                        @foreach ($products as $product)
                            <div class="my-wrapper">
                                <div class="product-img">
                                    <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"
                                         height="420" width="327"/>
                                </div>
                                <div class="product-info">
                                    <div class="product-text">
                                        <h1>{{$product->name}}</h1>
                                        <h2>by studio and friends</h2>
                                        <p>Harvest Vases are a reinterpretation<br> of peeled fruits and vegetables as<br>
                                            functional objects. The surfaces<br> appear to be sliced and pulled aside,<br>
                                            allowing room for growth. </p>
                                    </div>
                                    <div class="product-price-btn">
                                        <p><span>{{$product->price}}</span>€</p>

                                        <a href="{{ url('add-to-cart/'.$product->id) }}" role="button"><button type="button">Añadir</button></a>
                                    </div>
                                </div>
                            </div>
                            {{--                            <li class="product-title">{{$product->name}}</li>--}}
                            {{--                            <li class="product-price">{{$product->price}}</li>--}}

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{$products->links() }}
    </div>

    <style>
        .my-wrapper {
            height: 420px;
            width: 654px;
            margin: 50px auto;
            border-radius: 7px 7px 7px 7px;
            /* VIA CSS MATIC https://goo.gl/cIbnS */
            -webkit-box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
            -moz-box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
            box-shadow: 0px 14px 32px 0px rgba(0, 0, 0, 0.15);
        }

        .product-img {
            float: left;
            height: 420px;
            width: 327px;
        }

        .product-img img {
            border-radius: 7px 0 0 7px;
        }

        .product-info {
            float: left;
            height: 420px;
            width: 327px;
            border-radius: 0 7px 10px 7px;
            background-color: #ffffff;
        }

        .product-text {
            height: 300px;
            width: 327px;
        }

        .product-text h1 {
            margin: 0 0 0 38px;
            padding-top: 52px;
            font-size: 34px;
            color: #474747;
        }

        .product-text h1,
        .product-price-btn p {
            font-family: 'Bentham', serif;
        }

        .product-text h2 {
            margin: 0 0 47px 38px;
            font-size: 13px;
            font-family: 'Raleway', sans-serif;
            font-weight: 400;
            text-transform: uppercase;
            color: #d2d2d2;
            letter-spacing: 0.2em;
        }

        .product-text p {
            height: 125px;
            margin: 0 0 0 38px;
            font-family: 'Playfair Display', serif;
            color: #8d8d8d;
            line-height: 1.7em;
            font-size: 15px;
            font-weight: lighter;
            overflow: hidden;
        }

        .product-price-btn {
            height: 103px;
            width: 327px;
            margin-top: 17px;
            position: relative;
        }

        .product-price-btn p {
            display: inline-block;
            position: absolute;
            top: -13px;
            height: 50px;
            font-family: 'Trocchi', serif;
            margin: 0 0 0 38px;
            font-size: 15px;
            font-weight: lighter;
            color: #474747;
        }

        span {
            display: inline-block;
            margin-top: 1.2rem;
            font-family: 'Suranna', serif;
            font-size: 20px;
        }

        .product-price-btn button {
            float: right;
            display: inline-block;
            height: 50px;
            width: 176px;
            margin: 0 40px 0 16px;
            box-sizing: border-box;
            border: transparent;
            border-radius: 60px;
            font-family: 'Raleway', sans-serif;
            font-size: 14px;
            font-weight: 500;
            text-transform: uppercase;
            letter-spacing: 0.2em;
            color: #ffffff;
            background-color: #9cebd5;
            cursor: pointer;
            outline: none;
        }

        .product-price-btn button:hover {
            background-color: #79b0a1;
        }

    </style>

@endsection
